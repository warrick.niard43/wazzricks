<div class="wrapper row3">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Divers</h6>
    </div>
    <ul class="nospace group btmspace-80 overview">
      <li class="one_third">
        <article><a href="https://wazzricks.000webhostapp.com/category/ordinateurportable"><i class="fas fa-laptop"></i></a>
          <h6 class="heading"><a href="https://wazzricks.000webhostapp.com/category/ordinateurportable">Ordinateur portable</a></h6>
          <p>Nouvautés et recommandations sur les PC portables&hellip;</p>
          <footer><a href="https://wazzricks.000webhostapp.com/category/ordinateurportable">View Details <i class="fas fa-angle-right"></i></a></footer>
        </article>
      </li>
      <li class="one_third">
        <article><a href="https://wazzricks.000webhostapp.com/category/codesbonus"><i class="fas fa-cube"></i></a>
          <h6 class="heading"><a href="https://wazzricks.000webhostapp.com/category/codesbonus">codes bONUS</a></h6>
          <p>Petits codes à "copier/coller" en "HTML" et "CSS"&hellip;</p>
          <footer><a href="https://wazzricks.000webhostapp.com/category/codesbonus">View Details <i class="fas fa-angle-right"></i></a></footer>
        </article>
      </li>
      <li class="one_third">
        <article><a href="https://wazzricks.000webhostapp.com/category/formations"><i class="fas fa-graduation-cap"></i></a>
          <h6 class="heading"><a href="https://wazzricks.000webhostapp.com/category/formations">Formations</a></h6>
          <p>Découvrer les formations disponibles pour aprendre le codage&hellip;</p>
          <footer><a href="https://wazzricks.000webhostapp.com/category/formations">View Details <i class="fas fa-angle-right"></i></a></footer>
        </article>
      </li>
      <li class="one_third">
        <article><a href="https://wazzricks.000webhostapp.com/category/ordinateurbureau"><i class=" fas fa-desktop"></i></a>
          <h6 class="heading"><a href="https://wazzricks.000webhostapp.com/category/ordinateurbureau">Ordinateur bureau</a></h6>
          <p>Nouvautés et recomandations sur les tours, écrans et accessoires&hellip;</p>
          <footer><a href="https://wazzricks.000webhostapp.com/category/ordinateurbureau">View Details <i class="fas fa-angle-right"></i></a></footer>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#"><i class="fas fa-bug"></i></a>
          <h6 class="heading"><a href="#">Porta libero at scelerisque</a></h6>
          <p>Lorem ipsum et arcu mauris consequat semper vivamus lobortis consequat&hellip;</p>
          <footer><a href="#">View Details <i class="fas fa-angle-right"></i></a></footer>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#"><i class="fas fa-bicycle"></i></a>
          <h6 class="heading"><a href="#">Purus fusce consectetuer</a></h6>
          <p>Nisl in massa phasellus feugiat arcu sed lacinia egestas augue lorem posuere&hellip;</p>
          <footer><a href="#">View Details</a> <i class="fas fa-angle-right"></i></footer>
        </article>
      </li>
    </ul>
    <footer class="center"><a class="btn" href="https://wazzricks.000webhostapp.com/category/divers">Consulter<i class="fas fa-angle-right"></i></a></footer>
    <!-- ################################################################################################ -->
  </section>
</div>