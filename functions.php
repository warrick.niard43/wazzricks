<?php

function edn_theme_setup() {
      register_nav_menus(
            array(
                  'menu-header' => __( 'Menu Header', 'edn-theme' ),
                  'menu-footer' => __( 'Menu Footer', 'edn-theme' ),
            )
      );

      add_theme_support( 'post-thumbnails' );
      add_theme_support( 'automatic-feed-links' );
      add_theme_support( 'title-tag' );

      add_theme_support( 'custom-logo', array(
            'height' => 100,
            'width' => 350,
            'flex-height' => true,
            'flex-width' => true,
      ) );
}
   add_action( 'after_setup_theme', 'edn_theme_setup' );

   add_filter( 'get_the_archive_title', function ($title) {    
if ( is_category() ) {    
            $title = single_cat_title( '', false );    
      } elseif ( is_tag() ) {    
            $title = single_tag_title( '', false );    
      } elseif ( is_author() ) {    
            $title = '<span class="vcard">' . get_the_author() . '</span>' ;    
      } elseif ( is_tax() ) { //for custom post types
            $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
      } elseif (is_post_type_archive()) {
            $title = post_type_archive_title( '', false );
      }
return $title;    
  });
