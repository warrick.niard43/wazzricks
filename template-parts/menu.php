<div class="wrapper row1">
  <section class="hoc clear"> 
    <!-- ################################################################################################ -->
    <?php 
    wp_nav_menu(
      array( 
        'theme_location' => 'menu-header' ,
        'container'  => 'nav',
        'container_id'  => 'mainav',
      ) 
    );
    ?>
  
    <!-- ################################################################################################ -->
    <div id="searchform">
      <div>
        <form action="#" method="post">
          <fieldset>
            <legend>Quick Search:</legend>
            <input type="text" placeholder="Enter search term&hellip;">
            <button type="submit"><i class="fas fa-search"></i></button>
          </fieldset>
        </form>
      </div>
    </div>
    <!-- ################################################################################################ -->
  </section>
</div>