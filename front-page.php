<?php get_header()?>

<?php get_template_part( 'template-parts/header' );?>
  
<?php get_template_part( 'template-parts/menu' );?>


<?php get_template_part( 'template-parts/section/section-2' );?>


<?php the_content();?>


<?php get_template_part( 'template-parts/section/section-3' );?>

<?php get_template_part( 'template-parts/section/section-4' );?>

<?php get_template_part( 'template-parts/section/section-5' );?>

<?php get_template_part( 'template-parts/section/mes-derniers-articles' );?>



<?php get_template_part( 'template-parts/footer' );?>

<?php get_footer()?>  
