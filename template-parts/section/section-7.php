<div class="wrapper row4">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_third first">
      <h6 class="heading">Nos réseaux disponibles</h6>
      <p>Vous trouverez ici, mes résauc sociaux.</p>
      <p class="btmspace-30">(Actuellement indisponible)[<a href="#"><i class="fas fa-arrow-right"></i></a>]</p>
      <ul class="faico clear">
        <li><a class="faicon-dribble" href="#"><i class="fab fa-dribbble"></i></a></li>
        <li><a class="faicon-facebook" href="#"><i class="fab fa-facebook"></i></a></li>
        <li><a class="faicon-google-plus" href="#"><i class="fab fa-google-plus-g"></i></a></li>
        <li><a class="faicon-linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
        <li><a class="faicon-twitter" href="#"><i class="fab fa-twitter"></i></a></li>
        <li><a class="faicon-vk" href="#"><i class="fab fa-vk"></i></a></li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading"></h6>
      <ul class="nospace clear latestimg">
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
        <li><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/demo/100x100.png" alt=""></a></li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Connexion/Inscription</h6>
      <p class="nospace btmspace-15">Connecter-vous:</p>
      <form method="post" action="<?php echo get_template_directory_uri();?>">
        <fieldset>
          <legend>Newsletter:</legend>
          <input class="btmspace-15" type="text" value="" placeholder="Identifiant">
          <input class="btmspace-15" type="text" value="" placeholder="Email">
          <button type="submit" value="submit">INACTIF</button>
        </fieldset>
      </form>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>