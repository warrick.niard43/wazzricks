<?php get_header()?>

<?php get_template_part( 'template-parts/header' );?>
  
<?php get_template_part( 'template-parts/menu' );?>

<?php get_template_part( 'template-parts/sidebar-header' );?>


  <main class="hoc container clear"> 
    
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <?php the_content();?>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    
    <div class="clear"></div>
  </main>
<?php get_template_part( 'template-parts/footer' );?>

<?php get_footer()?>  
