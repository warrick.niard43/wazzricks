<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Mes Formateurs</h6>
      <p>Apprentissage Des Bases De Codage</p>
    </div>
    <div class="group center btmspace-80">
      <article class="one_third first"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Anthony</h6>
        <p>HTML/CSS</p>
      </article>
      <article class="one_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Kristen</h6>
        <p>JavaScript</p>
      </article>
      <article class="one_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Faouzi</h6>
        <p>PHP</p>
      </article>
    </div>
    <p class="center"><a class="btn" href="#">INACTIF<i class="fas fa-angle-right"></i></a></p>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>